<!doctype html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
  <meta charset="utf-8">
  <title>Angrerettskjema</title>
  <base href="<?php echo $base; ?>" />
  <link type="text/css" href="view/stylesheet/angre.css?v=1.0.0.1" rel="stylesheet" media="all" />
  <!--[if lt IE 9]>
    <script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>
<body>
<?php foreach ($orders as $order) { ?>
<?php
$box015 = "Se vedlagt fraktbrev";
$box018 = "";
$box020 = "";

$boxes = '
<input tabindex="010" type="text" class="box" id="box010" CHANGE value="'.$order['store_name'].'">
<input tabindex="011" type="text" class="box" id="box011" CHANGE value="'.$order['store_address'].'">
<input tabindex="012" type="text" class="box" id="box012" CHANGE value="'.$order['store_telephone'].'">
<input tabindex="013" type="text" class="box" id="box013" CHANGE value="'.$order['store_email'].'">
<input tabindex="014" type="text" class="box" id="box014" CHANGE value="'.$order['order_id'].'">
<input tabindex="015" type="text" class="box" id="box015" CHANGE value="'.$box015.'">
<input tabindex="016" type="text" class="box" id="box016" CHANGE value="'.$order['date_added'].'">
<input tabindex="017" type="text" class="box" id="box017" CHANGE value="'.date('d/m/Y').'">
<input tabindex="018" type="text" class="box" id="box018" CHANGE value="'.$box018.'">
<input tabindex="019" type="text" class="box" id="box019" CHANGE value="'.$order['customer_name'].'">
<input tabindex="020" type="text" class="box" id="box020" CHANGE value="'.$order['shipping_address'].'">
<input tabindex="021" type="text" class="box" id="box021" CHANGE value="'.$order['telephone'].'">
<input tabindex="022" type="text" class="box" id="box022" CHANGE value="'.$order['email'].'">
';
?>
<div id="skjema" class="front"><form><?php echo $boxes; ?><img src="view/image/angre_front.gif" class="form_image"></form></div>  
<div id="skjema" class="back"><img src="view/image/angre_back.gif" class="form_image"></div>
<?php } ?>
</body>
</html>
