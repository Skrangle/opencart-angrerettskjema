# README #

This VQMod adds a button to print a prefilled form when you view an order. 

### What is this repository for? ###

In Norway webshops are required to supply this form prefilled along with any orders they send out. If the form is blank, the free return time can be up to 12 months instead of 14 days.

* [Read more about the rules here](http://www.forbrukerradet.no/forside/angrer-du-pa-et-kjop/)

### How do I get set up? ###

Made for OC2.

Just extract archive and upload to you server. No files are replaced.
You should ALWAYS back up your data before modifying it.

### Contribution guidelines ###

There are some hardcoded Norwegian text lines in this extension. Feel free to change whatever you want.

### Who do I talk to? ###

Please test and give me some feedback if something does not work. Since we give this away for free we do not give you any guaranties for functionality. We also do NOT make customized versions of this mod. Hire a developer... 